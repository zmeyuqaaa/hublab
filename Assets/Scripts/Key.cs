﻿using UnityEngine;

public class Key : MonoBehaviour
{
    // public AudioSource GetKey;

    private void OnTriggerEnter(Collider other)
    {
        

        if (other.TryGetComponent(out Player player))
        {
            // GetKey.Play();
            
            player.PickUpKey();
            Destroy(gameObject);
        }
    }
}
