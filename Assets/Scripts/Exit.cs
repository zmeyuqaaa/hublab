using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(MeshRenderer))]
public class Exit : MonoBehaviour
{
    [SerializeField] private Material _closedMaterial;
    [SerializeField] private Material _openMaterial;

    public bool IsOpen { get; private set; }

    // public AudioSource OpenGate;

    private MeshRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
    }

    public void Open()
    {
        Debug.LogError("KeySound");
        // OpenGate.Play();
        // Debug.LogError("KeyMusic");
        IsOpen = true;
        _renderer.sharedMaterial = _openMaterial;
    }

    public void Close()
    {
        // OpenGate.Stop();
        IsOpen = false;
        _renderer.sharedMaterial = _closedMaterial;
    }
}
